package main

import (
	"log"
	"net"

	"../src/network"
)

func main() {

	log.Println("begin dial...")
	conn, err := net.Dial("tcp", ":8999")
	if err != nil {
		log.Println("dial error:", err)
		return
	}
	log.Println("dial ok")

	data := make([]byte, 0)
	msg := network.NewMessage(1, data)
	sendData, _ := network.Encode(msg)
	// conn.Write(sendData)

	for {
		var b = make([]byte, 1024)
		conn.Read(b)

		conn.Write(sendData)
	}

	conn.Close()
}
