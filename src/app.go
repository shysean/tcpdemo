package main

import (
	"log"
	"sync"
	"time"

	"./network"
)

var ss *network.SocketService

func main() {
	var err error
	ss, err = network.NewSocketService("127.0.0.1:8999")
	if err != nil {
		log.Println("main err", err)
		return
	}
	defer ss.Stop("stop")

	ss.RegOnMessageHandler(onMessage)
	ss.RegOnConnectHandler(onConnecnt)
	ss.RegOnDisconnectHandler(onDisconnect)

	ss.SetHeartBeat(2*time.Second, 10*time.Second)

	go ss.Serv()

	// ss.Stop("Stop right now")

	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}

func onMessage(uuid string, msg *network.Message) {
	log.Println("receive a message from ", uuid, msg.GetID())

	// ss.Unicast(uuid, msg)
	// ss.Broadcast(msg)
}

func onConnecnt(conn *network.Conn) {
	log.Println("onConnecnt:", conn.GetName(), ss.GetConnsCount())
}

func onDisconnect(conn *network.Conn, err error) {
	log.Println("onDisconnecnt:", conn.GetName(), err)
}
